#!/usr/local/bin/python
#-*- coding: utf-8-*-
# nombre: roberto altamirano martinez
# isx47262285
# descripcion:
#     exercicis de consultas xml 
# synopsis:
# 		
# Ejercicios de XPath
##########################################################################
from lxml import etree

arbol = etree.parse ("./ejercicios_xpath_documento.xml")
#1. Nombre de la Universidad.
'''
print arbol.getroot()[0].text
'''

'''
nombre = arbol.xpath("/universidad/nombre/text()")
print nombre[0]
'''

'''
nombre = arbol.findtext("nombre")
print nombre
''' 
# version 2.0 
'''
universidad = arbol.xpath('/universidad/nombre')

print universidad[0].text
'''


#2. Pais de la Universidad.
'''
pais = arbol.xpath('/universidad/pais')

print pais[0].text
'''

'''
pais = arbol.findtext("pais")
print pais
''' 
#3. Nombres de las Carreras.
'''
carreras = arbol.findall('/carreras/carrera/nombre')

for nombre in carreras:
	
	print nombre.text
'''
'''
carreras = arbol.findall("//carrera/nombre")

for nombre in carreras:
	
	print nombre.text
'''
#4. Años de plan de estudio de las carreras.
'''
plan = arbol.findall('/carreras/carrera/plan')

for anyo in plan:
	print anyo.text
'''

'''
carreras = arbol.findall("carreras/carrera")

for carrera in carreras:
	
	print '%s:  %s' % (carrera[0].text , carrera[1].text )

'''

'''
carreras = arbol.findall("carreras/carrera")

for carrera in carreras:
	
	print carrera.findtext("nombre") , carrera.findtext("plan")
'''
'''
carreras = arbol.xpath("//carrera/nombre | //plan")

for carrera in carreras:
	print carrera.text

print "-------------------------------------"
'''
#5. Nombres de todos los alumnos.
'''
alumnos = arbol.findall('alumnos/alumno/nombre')

for nombre in alumnos:
	print nombre.text
'''
'''
alumnos = arbol.xpath("//alumno/nombre")
for nombre in alumnos:
	print nombre.text
'''
#6. Identificadores de todas las carreras.
'''
identificador = arbol.findall('carreras/carrera')

for idt in identificador:
	print idt.attrib["id"]
'''
'''
carreras = arbol.findall('carreras/carrera')

for carrera in carreras:
	print carrera.attrib["id"] , carrera[0].text 
'''
# solucio profe
'''
identificador = arbol.xpath('//carreras/carrera[@id]')

for idt in identificador:
	print   idt.attrib["id"] , idt[0].text
'''


#7. Datos de la carrera cuyo id es c01.
'''
carrera = arbol.findall("carreras/carrera[@id='c01']")

print "id =" , carrera[0].attrib["id"]

for elementos in carrera[0]:
	
	print elementos.tag,': ', elementos.text
'''
# exemple profe cutre (no salen todos los elementos)
'''
count = 1
carreras = arbol.findall("carreras/carrera")

for carrera in carreras:
	
	print carreras[0].tag , count
	print "-----------------------------"
	print carrera[0].tag, "=> ", carrera[0].text
	print carrera[1].tag, "=>" , carrera[1].text
	print carrera[2].tag, "=>" , carrera[2].text
	print carrera[3].tag, "=>", carrera[3].text
	print 
	
	count +=1
'''	
# exemple profe pro 
# metodo getchildren() , getparent()
'''
count = 1
carreras = arbol.findall("carreras/carrera")

for carrera in carreras:
	
	print carreras[0].tag , count 
	print "-----------------------"
	
	fills  = carrera.getchildren()
	
	for elem in fills:
		print elem.tag , "=>", elem.text 
	
	print
	count += 1
'''




# version roberto cutre no acabada 

'''

el = 0
carreras = arbol.findall("carreras/carrera")

for carrera in carreras:
	
	if not carrera[el].tag:
		el = 0
		break 

	else:
		print carrera[el].tag,  "=>" , carrera[el].text
	el +=1
	
'''
#8. Centro en que se estudia de la carrera cuyo id es c02.
'''
carrera = arbol.findall("carreras/carrera[@id='c02']/centro")

for centro in carrera:
	print centro.text
'''
#9. Nombre de las carreras que tengan subdirector.
'''
carreras = arbol.findall('carreras/carrera/subdirector/../nombre')

for carrera in carreras:
	print carrera.text
'''
#10. Nombre de los alumnos que estén haciendo proyecto.


'''
# alumnos = arbol.xpath ('//alumno/estudios/proyecto/../../nombre')
alumnos = arbol.findall('alumnos/alumno/estudios/proyecto/../../nombre')

for nombre in alumnos:
	print nombre.text
'''

#11. Códigos de las carreras en las que hay algún alumno matriculado.
'''
carreras = arbol.findall('alumnos/alumno/estudios/carrera')

codigos = []

for carrera in carreras:
	codigo = carrera.attrib['codigo']
	
	if codigo not in codigos:
		codigos.append(codigo)
	
print codigos	
'''
#12. Apellidos y Nombre de los alumnos con beca.
'''
alumnos = arbol.findall("alumnos/alumno[@beca='si']")

for alumno in alumnos:
	print 'apellidos: ',alumno[0].text,alumno[1].text
	print 'nombre: ', alumno[2].text
'''
#13. Nombre de las asignaturas de la titulación c04.

'''
asignaturas = arbol.findall("asignaturas/asignatura[@titulacion='c04']")

for asignatura in asignaturas:
	print asignatura[0].text
'''

#14. Nombre de las asignaturas de segundo trimestre.
'''
asignaturas = arbol.findall('asignaturas/asignatura')

for asignatura in asignaturas:
	if asignatura[3].text == '2':
		print asignatura[0].text
'''
# version 2
'''	
asignaturas = arbol.xpath("//asignaturas/asignatura[trimestre='2']/nombre")

for asig in asignaturas:
	print asig.text
'''		
#15. Nombre de las asignaturas que no tienen 4 créditos teóricos.
'''
asignaturas = arbol.findall('asignaturas/asignatura')

for asignatura in asignaturas:
	if asignatura[1].text != '4':
		print asignatura[0].text
'''
'''
asignaturas = arbol.xpath("//asignaturas/asignatura[creditos_teoricos='4']/nombre")

for asig in asignaturas:
	print asig.text
'''
#16. Código de la carrera que estudia el último alumno.

'''
alumno = arbol.find('alumnos/alumno[last()]/estudios/carrera')

print alumno.attrib['codigo']
'''

#17. Código de las asignaturas que estudian mujeres.
'''
asignaturas = arbol.findall("alumnos/alumno[sexo='Mujer']/estudios/asignaturas/asignatura")


for asignatura in asignaturas:
	print asignatura.attrib
'''

'''
uni = etree.parse('ejercicios_xpath_documento.xml')
carreras = uni.xpath("/universidad/alumnos/alumno[sexo='Mujer']/estudios/asignaturas/asignatura")


for carrera in carreras:
	print carrera.attrib
'''

#18. Nombre de los alumnos que matriculados en la asignatura a02.

'''
alumnos = arbol.xpath("/universidad/alumnos/alumno//asignatura[@codigo='a02']/../../..")


for alumno in alumnos:
	print '%s %s, %s' % (alumno[0].text,alumno[1].text,alumno[2].text)
'''
'''
alumnos = arbol.xpath("//alumnos/alumno/estudios/asignaturas/asignatura[@codigo='a02']/../../..")
for alumno in alumnos:
	print '%s %s, %s' % (alumno[0].text,alumno[1].text,alumno[2].text)
'''
#19. Códigos de las carreras que estudian los alumnos matriculados en alguna asignatura.
'''
codigos = arbol.xpath("/universidad/alumnos/alumno//asignatura/../../carrera")

for codigo in codigos:
	print codigo.attrib['codigo']
'''

#20. Apellidos de todos los hombres.

'''
alumnos = arbol.findall("alumnos/alumno[sexo='Hombre']")

for alumno in alumnos:
	print '%s %s' % (alumno.findtext('apellido1'),alumno.findtext('apellido2'))
'''

#21. Nombre de la carrera que estudia Víctor Manuel. "u" per fer unicode y aceptar caracters especials.
'''
carreras = arbol.findall(u"alumnos/alumno[nombre='Víctor Manuel']/estudios/carrera")

for carrera in carreras:
	
	codigo = carrera.attrib['codigo']
	print codigo


carrera= arbol.findall("carreras/carrera[@id='%s']/nombre" % codigo)
print carrera[0].text
'''


# version corta sin for 
'''
carreras = arbol.findall(u"alumnos/alumno[nombre='Víctor Manuel']/estudios/carrera")

codigo = carreras[0].attrib['codigo']
print codigo 

carrera= arbol.findall("carreras/carrera[@id='%s']/nombre" % codigo)
print carrera[0].text
'''
#22. Nombre de las asignaturas que estudia Luisa.
'''
asignaturas = arbol.findall("alumnos/alumno[nombre='Luisa']/estudios/asignaturas/asignatura")

codigos = []

for asignatura in asignaturas:
	codigos.append(asignatura.attrib['codigo'])
print codigos

for codigo in codigos:
	print arbol.findall("asignaturas/asignatura[@id='%s']/nombre" % codigo)[0].text 
'''
#versio corta
'''
asignaturas = arbol.findall("//alumno[nombre='Luisa']//asignatura")
codigos = []

for asignatura in asignaturas:
	codigos.append(asignatura.attrib['codigo'])
print codigos

for codigo in codigos:
	print arbol.findall("//asignatura[@id='%s']/nombre" % codigo)[0].text 
	
'''
	
#23. Primer apellido de los alumnos matriculados en Ingeniería del Software. atencio amb la "u"
'''
asignatura = arbol.xpath(u"//asignaturas/asignatura[nombre='Ingeniería del Software']")

codigo = asignatura[0].attrib['id']

print codigo
alumnos = arbol.findall(u"alumnos/alumno//asignatura[@codigo='%s']/../../.." % (codigo))

for alumno in alumnos:
	print alumno.findtext('apellido1')
'''


#24. Nombre de las carreras que estudian los alumnos matriculados en la asignatura Tecnología de los Alimentos.
'''
asignatura = arbol.findall(u"asignaturas/asignatura[nombre='Tecnología de los Alimentos']")

codigo = asignatura[0].attrib['id']

alumnos = arbol.findall("alumnos/alumno//asignatura[@codigo='%s']/../../.." % (codigo))

for alumno in alumnos:
	print alumno.findtext('apellido1')
'''

#25. Nombre de los alumnos matriculados en carreras que no tienen subdirector.

carreras = arbol.xpath(u"//carreras/carrera[not(subdirector)]")
lista_codigos  =[]
for carrera in carreras:
	codigo = carrera.attrib['id']
	codigo.append('lista_codigos')
	

for codigo in lista_codigos:	
	alumnos = arbol.xpath("//alumnos/alumno/estudios/carrera[@codigo='%s']/../../nombre" % (codigo))
	for alumno in alumnos:
		print alumno[2].text



#26. Nombre de las alumnos matriculados en asignaturas con 0 créditos prácticos y que estudien la carrera de I.T. Informática .

#27. Nombre de los alumnos ques estudian carreras cuyos planes son anteriores a 2002.
