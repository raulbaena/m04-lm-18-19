Aplicar fulls d’estil a documents RSS
=====================================

MP4UF2A1T3

Us de CSS amb documents RSS

CSS amb vocabularis diferents de l’HTML
---------------------------------------

És possible utilitzar CSS amb qualsevol vocabulari XML, tenint en compte
aquests aspectes:

-   La vinculació entre document i full d’estil es fa amb una instrucció
    de processament, com per exemple:\
    `<?xml-stylesheet type="text/css" href="http://you.com/rss.css"?>`.
-   No ha hi full d’estil per defecte, i cal per tant definir sempre i
    sistemàticament la propietat `display` per tots els elements.
-   En el cas dels navegadors actuals, és possible que sí disposin d’un
    full d’estil per defecte per alguns vocabularis importants, com RSS.

Espais de noms
--------------

Encara que no tots els navegadors suporten la declaració d’espais de
noms en els fulls d’estil CSS, val la pena explorar el seu funcionament
en donar estil a documents RSS.

-   El principal tipus de selector CSS és el nom d’element, i si aquest
    està en un espai de noms el seu nom complet està format pel prefix,
    els dos punts i el nom de l’element.
-   Com que els dos punt formen part de la definició de les regles de
    CSS, apareix un problema que el W3C ha solucionat d’aquesta forma
    -   Cal declarar en els fitxers CSS els espais de noms amb la
        sentència, per
        exemple, `@namespace rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#";`.
    -   Com és evident, en cada declaració hem de triar el prefix i usar
        la URL apropiada al cas.
    -   Podem usar els espais de nom com prefix dels noms d’element amb
        la sintaxi `rdf|li { … }` usant la barra vertical en lloc dels
        dos punts.
-   Alguns navegadors suporten una solució no estàndard, consistent en
    escriure dins dels fitxers CSS els selectors així: `rdf\:li { … }`.

Enllaços recomanats
-------------------

-   [Wikipedia: Processing
    instruction](http://en.wikipedia.org/wiki/Processing_instruction)
-   [Associating Style Sheets with XML
    documents](http://www.w3.org/TR/xml-stylesheet/)
-   [ZVON: Atom Reference](http://zvon.org/comp/r/ref-Atom.html)
-   [ZVON: CSS 2.1 Reference](http://zvon.org/comp/r/ref-CSS_2_1.html)

Pràctiques
----------

-   Busca amb Google la cadena “*pretty Atom CSS*” i consulta els
    primers documents recuperats.
-   Aplica el full d’estil situat en el [repositori local](aux/atom.css)
    al canal Atom fet en dies anteriors.
-   Modifica el full d’estil situat en el [repositori
    local](aux/atom.css) per personalitzar-lo al teu gust.
-   Descarrega el canal de
    [Barrapunto](http://barrapunto.com/index.rss), edita i adapta una
    còpia del full d’estil situat en el [repositori local](aux/atom.css)
    i fes-lo servir per aplicar estil al canal.
-   Tradueix per escrit les declaracions d’espais de noms del fitxer
    descarregat de [Barrapunto](http://barrapunto.com/index.rss) a les
    mateixes declaracions fetes dins d’un fitxers CSS.

