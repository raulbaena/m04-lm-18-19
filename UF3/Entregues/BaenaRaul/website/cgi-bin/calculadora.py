#!/usr/bin/python
# Fem l'import
import cgi, cgitb 

#Creen la instancia de FieldStorage 
form = cgi.FieldStorage() 

#Rebrem les dades
num1 = form.getvalue('first_name')
num2 = form.getvalue('last_name')
signe = form.getvalue('signe')

#Funcio per calcular el resultat
def operacio(num1,num2,signe):
	if signe == '+':
		resultado = float(num1) + float(num2)
	if signe == '*':
		resultado = float(num1) * float(num2)
	if signe == '-':
		resultado = float(num1) - float(num2)
	if signe == '/':
		resultado = float(num1) / float(num2)
	return resultado

#Calculem el total
total = operacio(num1,num2,signe) 

#Creem la pagina
print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<title>Hello - Second CGI Program</title>"
print "</head>"
print "<body>"
print "<h1>El resultat es: %d</h1>" % (total) 
print "</body>"
print "</html>"


