#!/usr/bin/python
# Fem l'import
import cgi, cgitb 

#Creen la instancia de FieldStorage 
form = cgi.FieldStorage() 

#Rebrem les dades
comunitats = form.getvalue('comunitat')
total = 0

#Diccionari amb les comunitats autonomes 
dic_comunitat = {"ES-AN":8388107,"ES-AR":1308563,"ES-AS":1042608,"ES-CN":2101924,"ES-CB":582206,
"ES-CM":2041631,"ES-CL":2447519,"ES-CT":7522596,"ES-EX":1087778,"ES-GA":2718525,"ES-IB":1107220,
"ES-RI":315794,"ES-MD":6466996,"ES-MC":1464847,"ES-NC":640647,"ES-PV":2189534,"ES-VC":4959968}

dic_nom_com = {"ES-AN":"Andalucia","ES-AR":"Aragon","ES-AS":"Asturias","ES-CN":"Canarias","ES-CB":"Cantabria",
"ES-CM":"Castilla la mancha","ES-CL":"Castilla y leon","ES-CT":"Catalunya","ES-EX":"Extremadura","ES-GA":"Galicia","ES-IB":"Islas Baleares",
"ES-RI":"La Rioja","ES-MD":"Comunidad de Madrid","ES-MC":"Murcia","ES-NC":"Navarra","ES-PV":"Pais Vasco","ES-VC":"Comunidad Valenciana"}

#Calculem el nombre d'habitants
habitants = [dic_comunitat[k] for k in dic_comunitat if k in comunitats]
for habitant in habitants:
	total = total + habitant
	
#Creem la pagina
print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<title>Programa Comunitat</title>"
print "</head>"
print "<body>"
for comunitat in comunitats:
	print dic_nom_com[comunitat],':', dic_comunitat[comunitat], "<br>"
print "El numero d'habitants total es : %d</h1>" % (total) 
print "</body>"
print "</html>"
