#!/usr/bin/python
# Fem l'import
import cgi, cgitb 

#Creen la instancia de FieldStorage 
form = cgi.FieldStorage() 

#Rebrem les dades
contrasenya1 = form.getvalue('contrasenya1')
contrasenya2 = form.getvalue('contrasenya2')

if contrasenya1 != contrasenya2:
	print "Content-type:text/html\r\n\r\n"
	print "<html>"
	print "<head>"
	print "</head>"
	print "<body>"
	print "<meta http-equiv='refresh' content='0; http://localhost:8000/recuperar_pass.html' />"
	print "</body>"
	print "</html> "
else:
	print "Content-type:text/html\r\n\r\n"
	print "<html>"
	print "<head>"
	print "<title>Contrasenya canviada</title>"
	print "</head>"
	print "<body>"
	print "Contrasenya canviada!"
	print "</body>"
	print "</html>"
