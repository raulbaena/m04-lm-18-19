Processar formularis
====================

MP4UF3A1T3

Programació d’scripts

Mòdul `cgi` de Python
---------------------

Informació tècnica (pots ignorar-la de moment): estem usant de moment un
senzill servidor web que porta la llibreria de Python, que suporta CGIs
però no suporta en aquests la redirecció derivada d’usar la capçalera
`Location`.

Per aquestes pràctiques usarem el mòdul `cgi` de Python, que facilita el
processament de les dades que el script rep en les variables d’entorn i
la seva entrada estàndard. Aquest mòdul ofereix moltes funcionalitats,
de les que tan sols farem ús d’unes poques. Per començar cal usar
aquestes:

-   Seguim usant el servidor web com en anteriors activitats, i pots
    reaprofitar els formularis que ja has fet. Ara del que es tracta és
    de fer nous scripts per gestionar les dades del formulari.
-   Recorda que els scripts han de tenir l’atribut d’execució.
-   Pots fer que els scripts sigui cridats en els formularis (atribut
    `action` de l’element `form`), o fer-ho directament amb URLs com ara
    `http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8`.
-   Per ajudar a la depuració dels scripts usarem aquest codi:

        import cgitb; cgitb.enable()

    Una vegada depurats els errors comentarem aquesta línia per
    desactivar aquesta ajuda a la depuració.

-   Si el mòdul `cgitb` no captura els error ens caldrà executar
    l’script directament en el terminal. Per fer-ho definex les
    variables d’entorn que espera trobar i simplement executa’l:

        $ export REQUEST_METHOD=get
        $ export QUERY_STRING="a=1&b=2..."
        $ ./script.py

-   Altres problemes es poden entre millor si usem `curl` o `wget` per
    fer les peticions:

        $ curl 'http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8'

-   Per processar el formulari tots els scripts han de tenir un codi
    similar a aquest:

        import cgi
        form = cgi.FieldStorage()

-   L’objecte retornat per `cgi.FieldStorage()` és similar a un
    diccionari, però té mètodes extra i poden faltar-li d’altres.
-   Els mètodes extra d’aquest objecte s’agrupen en dos formes diferents
    de treballar:
    -   Mètode `form.getvalue("NOM-CAMP"[, default])` que pot retornar
        llistes o cadenes.
    -   Mètodes `form.getfirst("NOM-CAMP"[, default])` (sempre retorna
        una cadena) i `form.getlist("NOM-CAMP")` (sempre retorna
        una llista).
-   Tots els scripts han de tenir un codi similar a aquest, previ a
    qualsevol sortida a generar:

        sys.stdout.write("Content-Type: text/plain; charset=UTF-8\r\n")
        sys.stdout.write("\r\n")

Enllaços recomanats
-------------------

-   [Python 2.5 Reference
    Card](http://home.uchicago.edu/~gan/file/python.pdf) (imprimeix en
    paper aquest document, i conserva una versió
    [local](../../UF2/A2/aux/python-2-5.pdf))
-   [Python Quick Reference Card](http://rgruet.free.fr/) (versió
    [local](../../UF2/A2/aux/python-QR.pdf))
-   Copia local de la [Python v2.7
    documentation](file:///usr/share/doc/python-docs/html/index.html)
    (paquet RPM `python-docs`)
-   Mòdul `cgi` en The Python Standard Library

Pràctiques I (**practiques1.md**)
------------

-   Còpia el fitxer [test.py](aux/test.py) en el directori `cgi-bin` i
    visita’l amb el navegador: què fa? 
-   Verifica aquest script amb diferents navegadors:
    -   `Firefox`
		```
		Primerplat ==> Pizza
		usuari ==> adsasd	
		complement ==> Peperoni
		segonplat ==> Escalopa
		contrasenya ==> asdasdasd
		fname ==> asdasd
		postre ==> Yogurt
		custId ==> 3487
		Paisos ==> 3
		begudes ==> begudes		
		```
    -   `lynx`
    ```
    ←←←
	Primerplat ==> Pizza
	==> ['papafrita', '\t\r\nikkhj']
	usuari ==> kjkk
	complement ==> Beicon
	segonplat ==> Canelones
	contrasenya ==> jtyj
	fname ==> POPPO
	postre ==> Yogurt
	custId ==> 3487
	Paisos ==> 3
	begudes ==> Cocacola
    ```
    -   `elinks`
    ```
    Primerplat ==> Pizza
	usuari ==> kjkk
	complement ==> Beicon
	segonplat ==> Canelones
	contrasenya ==> jtyj
	fname ==> POPPO
	postre ==> Yogurt
	custId ==> 3487
	Paisos ==> 3
	begudes ==> Cocacola
    ```
    -   `curl`
    ```
    ```

    -	`wget`
    
		```
		--2019-02-27 09:06:16--  http://localhost:8000/
		Resolving localhost (localhost)... ::1, 127.0.0.1
		Connecting to localhost (localhost)|::1|:8000... failed: Connection refused.
		Connecting to localhost (localhost)|127.0.0.1|:8000... connected.
		HTTP request sent, awaiting response... 200 OK
		Length: 3678 (3.6K) [text/html]
		Saving to: ‘index.html.1’
		
		index.html.1                            100%[=============================================================================>]   3.59K  --.-KB/s    in 0s      
		
		2019-02-27 09:06:16 (529 MB/s) - ‘index.html.1’ saved [3678/3678]
			
		```
	
-   Executa aquest script directament en un terminal, definint les
    variables d’entorn necessàries. 
    
    ```
    Primer hem de depurarem els scrptis utilitzant aquests metod
		import cgitb; cgitb.enable()
	Si el modul no retorna cap error definirem les variables d'entorn
		export REQUEST_METHOD=get
		export QUERY_STRING="a=1&b=2..."
	Executem l'script i retorna el seguent resultat
		[isx53320159@i12 cgi-bin]$ ./test.py 
		Content-Type: text/plain; charset=UTF-8
		a ==> 1
		b ==> 2...
    ```
    
-   Provoca en aquest script errors de sintaxi i errors en
    temps d’execució. Què passa?.
    
    ```
	Per provocar l'error de execució he hagut de modificar el fitxer test.py
	He afegit la seguent linea
		Error
	Quan he executat l'script a aparegut el seguent error
		[isx53320159@i12 cgi-bin]$ ./test.py 
		Traceback (most recent call last):
		File "./test.py", line 17, in <module>
			Error
		NameError: name 'Error' is not defined
	Aixo vol dir que a la linea 17 ha detectat un error y no s'ha executat res
	Tambe pero comprobar el funcionament del script enviant una petitció amb
	la seguent comanda
		curl 'http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8'
	Ha sortit el seguent error
		127.0.0.1 - - [27/Feb/2019 09:43:33] "GET /cgi-bin/test.py?a=1&a=3&b=8 HTTP/1.1" 200 -
		Traceback (most recent call last):
		File "/home/users/inf/hisx2/isx53320159/website/cgi-bin/test.py", line 17, in <module>
			Error
		NameError: name 'Error' is not defined
		127.0.0.1 - - [27/Feb/2019 09:43:33] CGI script exit status 0x100
    ```
    
-   Visita aquest script amb alguns dels formularis
    realitzats anteriorment.
    
    Resultat fet amb l'arxiu index.hmtl
		Primerplat ==> Pizza
		usuari ==> dc
		complement ==> Pernillsalat
		segonplat ==> Canelones
		contrasenya ==> dc
		fname ==> cd
		postre ==> Creepe
		custId ==> 3487
		Paisos ==> 1
		begudes ==>  Aigua
    
-   Estudia la documentació del mòdul `cgi`, especialment les propietats
    de l’objecte retornat per `cgi.FieldStorage()`.
    
		FieldStorage.getlist(name) Retorna una llista de valors
		FieldStorage.getfirst(name[, default]) Aquest mètode sempre torna només un valor associat amb el nom del camp de formulari.
		El mètode getvalue() retorna aquest una cadena o una llista de valors
			
-   Prepara un formulari i modifica aquest script per explorar el
    serveis proporcionats per aquest object.
    
    

Pràctiques II (creeu el directori **practiques2**)
-------------

Aquests exercicis demanen presentar diferents formularis, i han de ser
processats per scripts que utilitzin el mòdul `cgi`.

-   Calculadora de les quatre operacions bàsiques (+, −, × i ÷), a ser
    presentades en un selector desplegable: per defecte ha de fer sumes,
    i si no es proporcionen operands o aquests no es poden convertir al
    tipus `float` cal usar els valors neutres de l’operació demanada. La
    resposta del script ha de ser un document HTML amb el resultat de
    l’operació i un enllaç per tornar a la calculadora.
-   Formulari amb selector múltiple per comunitats autònomes espanyoles:
    cal respondre amb el [nombre
    d’habitants](http://es.wikipedia.org/wiki/Demograf%C3%ADa_de_Espa%C3%B1a#Distribuci.C3.B3n_de_la_poblaci.C3.B3n)
    de cada
    [comunitat](http://es.wikipedia.org/wiki/Comunidad_aut%C3%B3noma) i
    la suma total dels mateixos (usa aquests
    [codis](http://es.wikipedia.org/wiki/ISO_3166-2:ES) per identificar
    les comunitats en el formulari).
-   Formulari per fer login: si la contrasenya no és *correcte* es còpia
    com a resposta de nou el formulari.
-   Formulari per canvi de contrasenya: demanar dues vegades nova
    contrasenya i verificar que són iguals; en cas d’error actuar com en
    el cas anterior.

