Processament de formularis
==========================

MP4UF3A1EP2

Exercici pràctic (EP2)

Característiques de l’exercici
------------------------------

Exercici pràctic.

### Tipus d’exercici

Aquest exercici servirà per mesurar les habilitats en el processament de
formularis HTML.

### Enunciat

El dia de realització de l’exercici el seu enunciat estarà disponible en
format XHTML en el [repositori local](_EP2.html) de fitxers.

### Criteri de qualificació

L’exercici aporta el 33% de la nota del resultat d’aprenentatge associat
a l’activitat.
